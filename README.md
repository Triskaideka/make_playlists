`make_playlists.perl` is a Perl script that puts together music or video playlist files in M3U format, based on settings that you specify in a JSON file.  It was designed to work with the [Kodi](https://osmc.tv/) media center software on the [OSMC](https://osmc.tv/) operating system, but should be easy to adapt to any environment.  The [M3U format](https://en.wikipedia.org/wiki/M3U) is simple and widely supported.

The Perl modules [Encode](https://metacpan.org/pod/Encode) and [JSON::Tiny](https://metacpan.org/pod/JSON::Tiny) are required.

I run this script once a day via cron, so that I can just drop new tracks into my media directories and they'll get added to the playlists automatically.  My crontab line looks like this:

    0 6 * * * /usr/bin/perl /home/osmc/bin/make_playlists/make_playlists.perl &> /home/osmc/bin/make_playlists/errors.txt

Be aware that Kodi already has [pretty sophisticated playlist support](http://kodi.wiki/view/Playlist), and depending on your needs, you may be better off using that.

# Settings

The script assumes that you've put its settings file at `/home/osmc/bin/make_playlists/playlists.json`.  If that's not true then you'll need to edit the line where `$settings_file` is assigned.

Start with the sample `playlists.json` that's included in this repository, and modify it as needed.  Here are the settings that you can configure:

## music_locs

An array of strings, which are (fully qualified) paths to the directories where you keep your music (or videos; don't get hung up on the name).  The script will start looking for media files in these directories, and continue into all of their subdirectories.

## playlist_dir

A string containing the (fully qualified) path to the directory where you want the playlist files to be saved.

## shuffle

A Boolean value, `true` if you want the script to randomize the tracks before printing out the playlist, `false` if you don't.  Note that Kodi will also let you shuffle playlists as you're playing them, so you may not need this.

## playlists

An object whose keys are the names of the playlist files, and whose values are sub-objects where you can configure additional settings that will be specific to this playlist.  In these sub-objects you can override the `shuffle`, `music_locs`, and `playlist_dir` settings, and you can also set a `blacklist`, which is an array of strings that name directories and files you want to leave *out* of the playlist.  While `music_locs` should contain fully qualified paths, the `blacklist` should not; it should only contain file and directory names.

# What counts as a music or video file?

This script looks at file extensions only.  A file that has an extension matching [any format that Kodi recognizes](http://kodi.wiki/view/Features_and_supported_formats) will be added to the playlist; any other files will be ignored.
