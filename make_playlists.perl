#!/usr/bin/perl -w

use warnings;
use strict;
use Encode;                      # https://metacpan.org/pod/Encode
use JSON::Tiny qw(decode_json);  # https://metacpan.org/pod/JSON::Tiny

# This list comes from http://kodi.wiki/view/Features_and_supported_formats plus some internet research
my @formats = qw~MIDI AIFF WAV WAVE AIFF MP2 MP3 AAC AAC+ OGG OGA AC3 DTS ALAC AMR FLAC APE RA SHN WV MPC MP+ MPP SPX WMA IT S3M MOD XM NSF SPC GYM SID IMF YM ADPCM CDDA~;
my $fmt_str = '\.(' . join('|', @formats) . ')$';
my $fmt_re = qr~$fmt_str~i;

# This directory won't automatically exist; you should create it, and/or edit this next line.  I use a full path instead of just
# a filename because I run this via cron and there's no guarantee that the current directory will be anything in particular.
my $settings_file = "/home/osmc/bin/make_playlists/playlists.json";
my ( @music_locs, $playlist_dir, $filename, $shuffle, $fh, %blacklist, @playlist );

# locally undefine the end-of-line delimiter so we can read in the whole settings file at once
local $/;

open (SETTINGS, "$settings_file") or die "Couldn't open $settings_file";
my $settings = decode_json <SETTINGS>;
close (SETTINGS);

foreach my $k (keys %{ $settings->{'playlists'} })  {

  # get the shuffle setting from within this playlist's settings, or else use the default shuffle setting, or else default to off
  $shuffle = ( exists($settings->{'playlists'}->{$k}->{'shuffle'}) ) ? 
                $settings->{'playlists'}->{$k}->{'shuffle'} :
                ( exists($settings->{'shuffle'}) ) ? 
                   $settings->{'shuffle'} :
                   0;

  # get the music locations from within this playlist's settings, or else use the default 
  # music locations, or else default to "/home/osmc/Music"
  @music_locs = ( exists($settings->{'playlists'}->{$k}->{'music_locs'}) ) ? 
                   @{ $settings->{'playlists'}->{$k}->{'music_locs'} } :
                   ( exists($settings->{'music_locs'}) ) ? 
                      @{ $settings->{'music_locs'} } :
                      ( "/home/osmc/Music" );

  # load the list of items to ban from this playlist, if any
  %blacklist = ();
  if ( exists($settings->{'playlists'}->{$k}->{'blacklist'}) )  {
    foreach my $item ( @{ $settings->{'playlists'}->{$k}->{'blacklist'} } )  {
      $blacklist{$item} = 1;
    }
  }

  # get the directory to save the playlist in from within this playlist's settings, or else use the default playlist
  # directory, or else default to "/home/osmc/.kodi/userdata/playlists/music"
  $playlist_dir = ( exists($settings->{'playlists'}->{$k}->{'playlist_dir'}) && -d $settings->{'playlists'}->{$k}->{'playlist_dir'} ) ? 
                     $settings->{'playlists'}->{$k}->{'playlist_dir'} :
                     ( exists($settings->{'playlist_dir'}) && -d $settings->{'playlist_dir'} ) ? 
                        $settings->{'playlist_dir'} :
                        "/home/osmc/.kodi/userdata/playlists/music";

  $filename = "$k.m3u";
  open($fh, ">:encoding(UTF-8)", "$playlist_dir/$filename") or die "Couldn't open $playlist_dir/$filename for writing";

  # write out the playlist file
  foreach my $item (@music_locs)  {
    @playlist = ();
    process($item);
    if ($shuffle)  { @playlist = shuffle(@playlist); }
    print $fh join "\n", @playlist;
  }

  close($fh);
}


sub process {
  my $thing = decode("utf8", shift);

  unless (-e $thing)  {
    print STDERR "Tried to process $thing but it doesn't exist [$!]\n";
  }

  # if it's a directory, recurse into it
  if (-d $thing) {
    opendir(my $dh, $thing) or die "Can't opendir $thing: $!";
    my @entries = sort { $a cmp $b } grep { !/^\./ } readdir($dh);
    closedir $dh;

    foreach my $e (@entries) {
      if ( ! defined($blacklist{$e}) )  {
        process( encode("utf8", $thing) . "/$e");
      }
    }
  }

  # if it's a file, check its file extension against the regex and add it if it passes
  if (-f $thing) {
    return unless $thing =~ $fmt_re;
    push @playlist, $thing;
  }
}


# Fisher-Yates shuffle
sub shuffle {
  my ($i, $j, $x);

  for ($i = 0; $i < @_ - 1; $i++) {
    $j = int(rand(@_ - $i)) + $i;
    $x = $_[$j];
    $_[$j] = $_[$i];
    $_[$i] = $x;
  }

  return @_;
}
